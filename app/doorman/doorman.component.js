angular
        .module('fiedlerDochazka')
        .component('doorman', {
            templateUrl: 'app/doorman/doorman.template.html',
            controller: 
                function (userProvider, records, exitReasonTranslate, groups, $interval, $rootScope, $state) {
                    var refresh;
                    var self = this;
                    self.overlay = false;

                    (function (attrs) {
                        //
                        // Pokud uživatel není schválen, přesměruje se na login, odkud je přesměrován kam patří
                        //
                        if ((attrs === undefined) || (!attrs.admin && !attrs.maintainer))
                            $state.go('login');
                    })($rootScope.user.attributes)

                    self.groups = groups.query();
                    self.groupfilter = [];

                    self.group = {};

                    self.setGroup = function (group) {
                        self.group = group;
                        self.setGroupFilter(group.attributes.members);
                    };

                    self.setGroupFilter = function (members) {


                        var filter = [];

                        members.forEach(function (item) {
                            filter.push(item.id);
                        });

                        self.groupfilter = filter;
                    };

                    self.exitReasonTranslate = exitReasonTranslate;
                    refresh = function () {
                        self.users = userProvider.advance({include: 'lastRecord'}, function () {
                            self.overlay = false;
                        });

                    };

                    refresh();

                    $interval(refresh, 60000);

                    self.enter = function (uid) {
                        self.overlay = true;
                        var record = new records;
                        record._userId = uid;
                        record.data = [
                            {
                                type: 'record',
                                attributes: {
                                    time: "",
                                    type: 0,
                                    direct: 1,
                                    edited: {
                                        type: 'group',
                                        id: self.group.id,
                                        time: ""
                                    }
                                }
                            }
                        ]
                        record.$save(refresh);
                    }

                    self.exit = function (uid, type) {
                        self.overlay = true;
                        var record = new records;
                        record._userId = uid;
                        record.data = [
                            {
                                type: 'record',
                                attributes: {
                                    time: "",
                                    type: type,
                                    direct: 0,
                                    edited: {
                                        type: 'group',
                                        id: self.group.id,
                                        time: ""
                                    }
                                }
                            }
                        ]
                        record.$save(refresh);
                    };
                }
        })