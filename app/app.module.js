'use strict';

// Declare app level module which depends on views, and components
angular.module('fiedlerDochazka', [
  'ngResource',
  'ngAnimate',
  'ngTouch',
  'ngCookies',
  "userStats",
  "ui.router",
  "ui.bootstrap",
  'datetime',
  'angular-jwt',
  'as.sortable',
  'contenteditable'
]);
