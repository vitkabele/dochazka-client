'use strict';

angular
        .module("fiedlerDochazka")
        .config([
            "$urlRouterProvider",
            "$locationProvider",
            "$stateProvider",
            function ($urlRouterProvider, $locationProvider, $stateProvider) {
                moment.locale('cs');

                $locationProvider.hashPrefix("!");

                /////////////////////////////
                // Redirects and Otherwise //
                /////////////////////////////

                // Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
                $urlRouterProvider

                        // The `when` method says if the url is ever the 1st param, then redirect to the 2nd param
                        // Here we are just setting up some convenience urls.
                        .when('/stats', '/stats/')

                        .otherwise("/login");

                //////////////////////////
                // State Configurations //
                //////////////////////////

                // Use $stateProvider to configure your states.
                $stateProvider
                        .state("login", {
                            ///////////
                            // Entry //
                            ///////////
                            // Vstupní bod celé aplikace, je v podstatě zbytečný, ale proč by se nemohlo username a hash zadávat hezky

                            // Use a url of "/" to set a state as the "index".
                            url: "/login",
                            template: '<entry-point></entry-point>'
                        })
                        .state("stats", {
                            templateUrl: "app/user-stats/app-layout.template.html"
                        })
                        .state("stats.overview", {
                            url: "/dashboard",
                            templateUrl: "app/user-stats/overview.template.html",
                            controller: [
                                'usersStorage',
                                'displayPreferences',
                                'statsStorage',
                                function (usersStorage, displayPreferences, statsStorage) {
                                    this.users = usersStorage.getAll();
                                    this.displayPreferences = displayPreferences;
                                    this.statsStorage = statsStorage;
                                }
                            ],
                            controllerAs: '$ctrl'
                        })
                        .state("stats.dashboard", {
                            /////////////////
                            /// Statistiky //
                            /////////////////
                            // Tady budou k nahlédnutí Statistiky jedince

                            url: '/stats/:viewedUserId',
                            template: '<dashboard></dashboard>'
                        })
                        .state("stats.daily", {
                            url: "/daily",
                            template: "<daily-dashboard></daily-dashboard>"
                        })
                        .state("stats.settings", {
                            url: "/settings",
                            template: "<settings></settings>"
                        })
                        .state("stats.present", {
                            url: "/present",
                            template: "<present></present>"
                        })
                        .state("error", {
                            //////////////////////
                            ////// Error view ////
                            //////////////////////

                            url: '/error/:eid',
                            template: '<error-view></error-view>'
                        })
                        .state('doorman', {
                            url: '/doorman',
                            template: '<doorman></doorman>'
                        })

            }
        ])

        .config([
            '$httpProvider',
            'jwtInterceptorProvider',
            function ($httpProvider, jwtInterceptorProvider) {

                jwtInterceptorProvider.tokenGetter = ['$cookies', function ($cookies) {
                        return $cookies.get('apiToken');
                    }];

                $httpProvider.interceptors.push('jwtInterceptor');
            }])

        .config(function ($httpProvider) {

            $httpProvider.interceptors.push(function ($q, $rootScope) {
                return {
                    response: function (response) {
                        var date = response.headers("ServerDate");

                        if(date)
                            $rootScope.serverTime = moment( date, "YYYY-MM-DD HH:mm:ss" ).toDate();

                        return response;

                    }
                };
            });
        });
