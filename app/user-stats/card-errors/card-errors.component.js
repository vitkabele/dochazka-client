'use strict';

angular
        .module('userStats')
        .component('cardErrors', {
            templateUrl: 'app/user-stats/card-errors/card-errors.template.html',
            bindings: {
                user:   '=',
                start:  '=',
                end:    '='
            },
            controller: [
                'statsStorage',
                function (statsStorage) {
                    this.statsStorage = statsStorage;
                }
            ]
        })
        .filter("errorCountTotal", function () {
            return function (data) {
                var errors = 0;

                if (data.data !== undefined)
                data.data.forEach(function (item) {
                    errors += item.attributes.warnings;
                });

                return errors;
            };
        })
        .filter("errorCountDays", function () {
            return function (data) {
                var inDays = 0;

                if (data.data !== undefined)
                data.data.forEach(function (item) {
                    if (item.attributes.warnings)
                        inDays++;
                });


                return inDays;
            };
        });