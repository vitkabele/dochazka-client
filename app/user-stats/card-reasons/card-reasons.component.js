'use strict';

angular
        .module('userStats')
        .component('cardReasons', {
            templateUrl: 'app/user-stats/card-reasons/card-reasons.template.html',
            bindings: {
                user:   '=',
                start:  '=',
                end:    '='
            },
            controller: [
                'statsStorage',
                'exitReasonTranslate',
                function (statsStorage, translateTable) {
                   this.reasonNames = translateTable;
                   this.statsStorage = statsStorage;
                }
            ]
        })
        .filter("tableReasons", function () {
            return function (data) {
                var accu = {};
                
                if(data.data !== undefined)
                data.data.forEach(function (item) {
                    item.meta.reasons.reduce(function (acc, curr) {
                        accu[curr] ? accu[curr]++ : accu[curr] = 1;
                        return;
                    }, {});
                });

                return accu;
            };
        });