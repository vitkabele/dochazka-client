'use strict';

var module = angular.module("userStats");

module.component("present",{
  templateUrl: "app/user-stats/present/present.template.html",
  controller: function(userProvider, $interval){
    var self = this;
    var refresh = function () {
        self.users = userProvider.advance({include: 'lastRecord'});
    };

    refresh();

    $interval(refresh, 30000);
  }
}).filter("presentFilter", function() {
  return function(users){
    var online = [];

    users.forEach(function(user){
      if( user.related.last_record.attributes.direct ) online.push(user);

    })

    return online;
  }
})
