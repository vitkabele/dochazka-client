'use strict';

angular
        .module('userStats')
        .component('settings', {
            templateUrl: 'app/user-stats/settings/settings.template.html',
            controller: [
                '$rootScope',
                '$scope',
                'userProvider',
                '$window',
                'groups',
                function ($rootScope, $scope, userProvider, $window, groups) {

                    var self = this;
                    
                    if( $rootScope.user.id )
                        self.actualUser = userProvider.get({userId: $rootScope.user.id});

                    var reloadGroups = function () {
                        self.groups = groups.query();
                    }
                    reloadGroups();

                    var reloadUsers = function () {
                        self.users = userProvider.query();
                    };

                    reloadUsers();

                    $scope.$on('usersReload', reloadUsers);

                    self.newUser = {
                        id: "",
                        type: "employee",
                        attributes: {}
                    };

                    self.deleteUser = function (user) {

                        if ($window.confirm("Opdavdu si přejete smazat uživatele?"))
                            user.$delete(function () {
                                $scope.$emit('usersReload');
                            });

                    }

                    this.comparePasswords = function () {
                        self.mismatchedPassword = (self.newUser.attributes.password !== self.newUser.attributes.password2);
                    }

                    this.createUser = function () {

                        var provider = new userProvider;
                        provider.data = [];
                        delete self.newUser.attributes.password2;
                        provider.data.push(self.newUser);
                        provider.$save(function () {
                            $scope.$emit('usersReload');
                        });
                    }


                    self.updateFormSubmit = function () {
                        self.actualUser.id = self.actualUser.data.id;
                        self.actualUser.$update();
                    }

                    self.listListeners = {
                        clone: true
                    };

                    self.sortableListeners = {
                        allowDuplicates: false
                    };

                    self.saveLists = function () {
                        self.groups.forEach(function (item) {
                            item.$update();
                        });
                    }
                    
                    self.addGroup = function(){
                        
                        var g = new groups;
                        g.attributes = {
                            name: "Nová skupina",
                            members: []
                        }
                        
                        g.$save(function(){
                            reloadGroups();
                        });
                        
                    }
                    
                    self.rmGroup = function( group ){
                        
                        if( $window.confirm("Opdavdu chcete smazat skupinu "+ group.attributes.name + "?") )
                        group.$delete(function(){
                            reloadGroups();
                        })
                        
                    }

                    self.trash = [];
                }
            ]
        })