/**
 * 
 * @author Vit Kabele <vit@kabele.me>
 */
'use strict';

var module = angular.module('userStats');
//
//  Tady se ukládají předvolby zobrazení
//  Nahrazuje period storage
//
module.service("displayPreferences", function () {
    this.period = {
        start: moment(),
        end: moment()
    };
    // Rozlišuje, zda budou v grafu zobrazeny denní, týdenní, nebo měsíční souhrny
    // Possible values: daily, weekly, monthly
    this.graphSummary = 'daily';
});
//
// Úložiště statistik
// Objekt stats by měl po inicializaci obsahovat nějakou základní kostru dat, aby se to dalo testovat
// Objekt má metodu na získání statistik, který spolupracuje s period storage.
// Statistiky ze serveru se získají až teprve, když budou vyžadovány.
// Pokud budou uložené statistiky starší než +/-30 minut (určí se), tak se načtou znovu, jinak se uloží a vrátí se bez dotazu
// Časem se může kombinovat s localStorage
//
module.service("statsStorage", [
    "statistiky",
    function (statistiky) {
        var self = this;
        this.stats = {
            data: [
            ],
            meta: {
                time_elapsed: 0
            }
        };
        /**
         * Sem se ukládají statistiky jednotlivých uživatelů
         * {
         *      user:   integer,
         *      start:  moment,
         *      end:    moment,
         *      stats:  {...}   // Ze serveru
         * }
         */
        this.storage = {};
        /**
         * Vrací statistiky pro uživatele
         * @param {integer} uid ID uživatele
         * @param {Moment} start Počátek periody
         * @param {Moment} end Konec periody
         * @returns {Object}
         */
        this.getStats = function (uid, start, end) {
            // Jsou uložené statistiky pro uživatele + pro stejný termín?
            if (self.storage[uid] && self.storage[uid].start.isSame(start, 'day') && self.storage[uid].end.isSame(end, 'day'))
            {
                return self.storage[uid].stats;
            } else {

                self.storage[uid] = {
                    user: uid,
                    start: start,
                    end: end,
                    stats: statistiky.get({
                        userId: uid,
                        start: start.format('YYYY-MM-DD'),
                        end: end.format('YYYY-MM-DD')
                    })
                };
                return self.storage[uid].stats;

            }
            ;
        };

        this.refresh = function (uid) {
            if (self.storage[uid])
            {
                var start = self.storage[uid].start, end = self.storage[uid].end;
                delete self.storage[uid];
                self.getStats(uid, start, end);
            }
        };

    }]);
//
//  Sumarizuje data pro graf
//
module.service("graphSummarizer", function (round) {
    //
    //  Formát dat který se vrací. Do něj se doplňuje.
    //
    this.summarized = function () {
        return {
            labels: [],
            series: [[]]
        };
    };
    //
    //  Denní souhrny 
    //
    this.daily = function (data) {
        var s = this.summarized();
        data.forEach(function (item) {
            
            var m = moment(item.id);
            s.series[0].push( round.round( moment.duration(item.attributes.total, 'seconds').asHours(), 1) );

            var label = m.format('dd');
            // V pondělí bude pod datumem napsáno pondělí
            if (m.format('d') == 1)
                label += '<br/>' + m.format('D.M.');

            s.labels.push(label);
        });
        return s;
    };
    //
    //  Týdenní souhrny
    //
    this.weekly = function (data) {
        var s = this.summarized();
        var weeks = [];
        data.forEach(function (item) {
            var m = moment(item.id);

            if (weeks.length === 0 || m.isoWeekday() === 1) {
                weeks.push({
                    start: item.id,
                    end: m.add(7 - m.isoWeekday(), 'days').format('YYYY-MM-DD'), //Přidá automaticky neděli jako konec týdne
                    total: 0
                });
            }

            weeks[ weeks.length - 1 ].total += item.attributes.total;
        });

        weeks.forEach(function (item) {
            s.labels.push(moment(item.start).format('D.M.') + ' - ' + moment(item.end).format('D.M.'));
            s.series[0].push( round.round( moment.duration(item.total, 'seconds').asHours(), 1 ));
        });
        return s;
    };
    //
    //  Měsíční souhrny
    //
    this.monthly = function (data) {
        var s = this.summarized();
        var months = [];
        data.forEach(function (item) {
            var m = moment(item.id);

            if (months.length === 0 || m.format('D') == 1) {
                months.push({
                    start: item.id,
                    end: m.endOf('month').format('YYYY-MM-DD'), //Přidá automaticky neděli jako konec týdne
                    total: 0
                });
            }

            months[ months.length - 1 ].total += item.attributes.total;
        });

        months.forEach(function (item) {
            s.labels.push(moment(item.start).format('MMMM'));
            s.series[0].push( round.round( moment.duration(item.total, 'seconds').asHours(), 1 ) );
        });
        return s;
    };

});
//
//  Obstarává seznam uživatelů
//
module.service("usersStorage", [
    'userProvider',
    function (userProvider) {
        var self = this;
        // Sem se ukládají uživatelé
        // id => Resource objekt
        this.storage = [];
        //
        //Získává jednoho uživatele
        //
        this.getUser = function (id) {
            // Když není načtený, načte do úložiště
            if( !self.storage[id] ) self.storage[id] = userProvider.get({ userId: id });
            return self.storage[id];
        };
        //
        //  Získává kompletní seznam
        //
        this.getAll = function () {
            return userProvider.query();
        };
    }]);

//
//  Obstarává seznam uživatelů
//
module.service("groupsStorage", [
    'groups',
    function (groups) {
        var self = this;
        // Sem se ukládají uživatelé
        // id => Resource objekt
        this.storage = [];
        //
        //Získává jednoho uživatele
        //
        this.getGroup = function (id) {
            // Když není načtený, načte do úložiště
            if( !self.storage[id] ) self.storage[id] = groups.get({ gid: id });
            return self.storage[id];
        };
        //
        //  Získává kompletní seznam
        //
        this.getAll = function () {
            return groups.query();
        };
    }]);

//
//  Obstarává seznam uživatelů
//
module.service("dailyStorage", 
    function (apiPrefix, $resource) {
        var connection = $resource( apiPrefix + 'days/:date' );
        
        var self = this;
        // Sem se ukládají uživatelé
        // id => Resource objekt
        this.storage = [];
        //
        //Získává jednoho uživatele
        //
        this.get = function (id) {
            // Když není načtený, načte do úložiště
            if( !self.storage[id] ) self.storage[id] = connection.get({ date: id });
            return self.storage[id];
        };
        //
        //
        //
        this.flush = function(){
            self.storage = [];
        }
    });
    
module.service("round", function(){
    this.round = function(number, precision){
        precision = precision || 2;
        var decimal = Math.pow(10, precision);
        return Math.round( number * decimal )/decimal;
    };
});