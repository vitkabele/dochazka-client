'use strict';

var module = angular
        .module('userStats');

        module.component('dailyDashboard', {
            templateUrl: "app/user-stats/daily-dashboard/daily-dashboard.template.html",
            controller: function(displayPreferences, dailyStorage){
                this.periodStorage = displayPreferences.period;
                dailyStorage.flush();
                
            }
        });
        

module.filter("daysFromInterval", function($resource){
    
    return function(periodStorage){
        
        var stats = $resource('api/v1/days/:date');
        var days = [], start = periodStorage.start.clone(), end = periodStorage.end.clone();
        
        for( ; end.diff(start, 'days') >= 0 ; start.add(1, 'days') ){
            days.push( start.format("YYYY-MM-DD") )
        }
        
        return days.reverse();
        
    };
    
});

module.filter("daily", function(dailyStorage){
    
    return function(date){
        
        var d = dailyStorage.get(date);
        return d;
    }
    
});