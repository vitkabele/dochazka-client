'use strict';

var module = angular.module('userStats');

module.component("cardDailyTotal",{
    templateUrl: "app/user-stats/card-daily-total/card-daily-total.template.html",
    bindings: {
        date: "="
    },
    controller: function(usersStorage, dailyStats){
        
    }
});