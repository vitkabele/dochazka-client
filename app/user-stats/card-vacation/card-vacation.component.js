/**
 * Obsluhuje kartu se zobrazením počtu dní dovolené
 * @author Vit Kabele <vit@kabele.me>
 */
'use strict';

angular
        .module('userStats')
        .component('cardVacation', {
            templateUrl: 'app/user-stats/card-vacation/card-vacation.template.html',
            bindings: {
                user:   '=',
                start:  '=',
                end:    '='
            },
            controller: [
                'statsStorage',
                function (statsStorage) {
                    this.statsStorage = statsStorage;
                }
            ]
        })
        .filter('vacationSummarizer', function () {
            return function (stats) {
                var vacation = 0;

                if(stats.data !== undefined)
                stats.data.forEach(function (item) {
                    
                    vacation += item.attributes.vacation && ( moment(item.id).isoWeekday() < 6 ) ;

                });

                return vacation;
            };
        });