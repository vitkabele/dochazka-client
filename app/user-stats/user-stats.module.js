'use strict';

angular
        .module("userStats", [])

        .constant('apiPrefix', '/api/v1/')
        .constant('exitReasonTranslate', {
              0: "Standartní",
            100: "Služební",
            200: "Dovolená",
            201: "Doktor",
            202: "Nemoc",
            302: "Osobní důvody",

        })
        .constant('holidays', [
            '1.1.',
            '25.3.',
            '17.4.',
            '1.5.',
            '8.5.',
            '5.7.',
            '6.7.',
            '28.9.',
            '28.10.',
            '17.11.',
            '24.12.',
            '25.12.',
            '26.12.'
        ])


        /**
         * Run block k ověření identity uživatele
         * @param {type} $rootScope
         * @param {type} $interval
         * @param {type} $cookies
         * @param {type} jwtHelper
         * @returns {undefined}
         */

        .run([
            '$rootScope',
            '$interval',
            '$cookies',
            '$state',
            'jwtHelper',
            'userProvider',
            function ($rootScope, $interval, $cookies, $state, jwtHelper, userProvider) {

                // Případ, že je stránka načtená přímo a má platný token
                if ($cookies.get('apiToken') && !jwtHelper.isTokenExpired($cookies.get('apiToken')) && !$rootScope.user)
                {
                    $rootScope.user = userProvider.get({
                        userId: jwtHelper.decodeToken($cookies.get('apiToken')).id},
                            function (response) {
                                $rootScope.user = response.data;
                            });
//                    $rootScope.viewedUserId = $rootScope.user.id;
                }

                var checkLogin = function () {
                    if (!$cookies.get('apiToken') || jwtHelper.isTokenExpired($cookies.get('apiToken'))) {
                        $cookies.remove('apiToken');
                        $state.go('login');
                    }

                };

                checkLogin();

                $rootScope.$on('$stateChangeSuccess', checkLogin);

                $interval(checkLogin, 5000);
            }
        ])

        /**
         * Tohle je inicializace aplikace,
         * měly by tady probíhat včechny operace, které jsou nezávislé na stavu
         *
         * Spravuje stav svých proměnných
         *
         * @param $rootScope
         * @param user
         * @returns app
         */
        .run([
            '$rootScope',
            'statistiky',
            'displayPreferences',
            '$interval',
            'statsStorage',
            'userProvider',
            function ($rootScope, statistiky, displayPreferences, $interval, statsStorage, userProvider) {
                //
                //  Id prohlíženého uživatele
                //
                $rootScope.viewedUserId = 0;
                //
                // Po změně prohlíženého uživatele se načtou jiné statistiky
                //
                $rootScope.$watch('viewedUserId', function () {
                    $rootScope.$broadcast('statsReloadDemand');
                });

                var statsReload = function () {
                    //
                    //  $rootScope.viewedUserId !== 0; To je aby se nenačítaly statistiky pokud je viewedUserId na výchozí hodnotě
                    //
                    if ($rootScope.viewedUserId > 0)
                        statistiky.get({
                            userId: $rootScope.viewedUserId,
                            start: displayPreferences.period.start.format('YYYY-MM-DD'),
                            end: displayPreferences.period.end.format('YYYY-MM-DD')
                        }, function (response) {
                            statsStorage.stats = response;
                            $rootScope.$broadcast('statsUpdated', response);
                        });
                };

                statsReload();

                $rootScope.$on('statsReloadDemand', statsReload);

                var tick = function () {
                    if ($rootScope.serverTime !== undefined)
                        $rootScope.serverTime = new Date($rootScope.serverTime.getTime() + 1000);
                }
                tick();
                $interval(tick, 1000);
            }])

        /**
         * Controller: navbarController
         * Stará se o zobrazení horní lišty v pohledu dashboard
         */
        .controller("navbarController", [
            '$scope',
            '$rootScope',
            '$cookies',
            '$state',
            '$uibModal',
            'displayPreferences',
            '$interval',
            function ($scope, $rootScope, $cookies, $state, $uibModal, displayPreferences, $interval) {

                var $_scope = $scope, self = this;

                $scope.periodOptions = (function () {

                    var options = [
                        {
                            title: "Poslední týden",
                            duration: {
                                start: moment().subtract(8, 'days').format('YYYY-MM-DD'),
                                end: moment().format('YYYY-MM-DD')
                            },
                            icon: 'ti-arrow-left'
                        }, {
                            title: "Tento měsíc",
                            duration: {
                                start: moment().startOf('month').format('YYYY-MM-DD'),
                                end: moment().format('YYYY-MM-DD')
                            },
                            icon: 'ti-timer'
                        }, {
                            title: "Letos",
                            duration: {
                                start: moment().startOf('year').format('YYYY-MM-DD'),
                                end: moment().format('YYYY-MM-DD')
                            },
                            icon: 'ti-list-ol'
                        }
                    ]

                    var month = moment();

                    for (var i = 1; i < moment().format('M'); i++) {
                        month.subtract(1, 'month');
                        options.push({
                            title: month.format('MMMM'),
                            duration: {
                                start: month.startOf('month').format('YYYY-MM-DD'),
                                end: month.endOf('month').format('YYYY-MM-DD')
                            },
                            icon: 'ti-calendar'
                        })
                    }

                    return options;
                })();

                $scope.changePeriod = function (optionId) {

                    var s = $scope.periodOptions[optionId];

                    displayPreferences.period.start = moment(s.duration.start);
                    displayPreferences.period.end = moment(s.duration.end);

                    $scope.actualPeriod = s;

                    $scope.$emit('statsReloadDemand');
                }

                $scope.openModal = function () {
                    $uibModal.open({
                        templateUrl: 'app/user-stats/period-modal.template.html',
                        controller: [
                            '$uibModalInstance',
                            '$scope',
                            'displayPreferences',
                            function ($uibModalInstance, $scope, displayPreferences) {
                                $scope.yesterday = moment().toDate();
                                $scope.duration = {
                                    start: displayPreferences.period.start.toDate(),
                                    end: displayPreferences.period.end.toDate()
                                };

                                $scope.save = function () {
                                    displayPreferences.period.start = moment($scope.duration.start);
                                    displayPreferences.period.end = moment($scope.duration.end);
                                    $_scope.actualPeriod = {
                                        title: "Vlastní",
                                        icon: "ti-pencil"
                                    }
                                    $scope.$emit('statsReloadDemand');
                                    $uibModalInstance.close();
                                };

                                $scope.cancel = function () {
                                    $uibModalInstance.dismiss();
                                }

                            }
                        ]
                    })
                }

                $scope.changePeriod(0);

                $scope.logout = function () {
                    $cookies.remove('apiToken');
                    $rootScope.user = {};
                    $state.go('login');
                }

            }])

        /**
         * Controller: sideBarController
         * Stará se o pohled boční lišty
         */
        .controller("sideBarController", [
            "$scope",
            "usersStorage",
            function ($scope, usersStorage) {

                $scope.users = usersStorage.getAll();

                var colors = [
                    "primary",
                    "info",
                    "success",
                    "warning",
                    "danger"
                ];

                $scope.sidebarColor = colors[Math.floor(Math.random() * colors.length)]

            }
        ])



///////////////////////
/// Sekce komponent ///
///////////////////////

        /**
         * Component: dashboard
         * Je to základní komponenta nástěnky, v ní se potom zobrazují jednotlivé informační prvky
         */
        .component("dashboard", {
            templateUrl: "app/user-stats/dashboard.template.html",
            controller:
                    function ($stateParams, $rootScope, displayPreferences, usersStorage, $state) {

                        (function (attrs) {
                            //
                            // Pokud uživatel není schválen, přesměruje se na login, odkud je přesměrován kam patří
                            //
                            if( (attrs === undefined) || (!attrs.admin && attrs.maintainer)  )
                                $state.go('login');
                        })($rootScope.user.attributes)

                        this.displayPreferences = displayPreferences;
                        $rootScope.viewedUserId = $stateParams.viewedUserId; // || $rootScope.user.id;
                        this.user = usersStorage.getUser($rootScope.viewedUserId);

                    }
        });
