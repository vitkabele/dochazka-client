'use strict';

angular
        .module('userStats')
        .factory('statistiky', [
            '$resource',
            'apiPrefix',
            function ($resource, apiPrefix) {
                return $resource(apiPrefix + 'employees/:userId/stats/:start/:end');
            }
        ])
        .factory('userProvider', [
            'apiPrefix',
            '$resource',
            function (apiPrefix, $resource) {
                return $resource(apiPrefix + 'employees/:userId', {userId: '@id'},{
                    advance: {
                        method: 'GET',
                        isArray:true
                    },
                    update: {
                        method: 'PUT'
                    },
                    delete: {
                        isArray: true,
                        method: 'DELETE'
                    }
                });
            }
        ])
        .factory('records', [
            'apiPrefix',
            '$resource',
            function (apiPrefix, $resource) {
                return $resource(apiPrefix + 'employees/:userId/records/:rid', {userId: '@_userId', rid: '@id'}, {
                    update: {method: 'PUT'}
                });
            }
        ])
        .factory('dailyStats', [
            'apiPrefix',
            '$resource',
            function (apiPrefix, $resource) {
                return $resource(apiPrefix + 'employees/:userId/days/:date');
            }
        ])
                .factory('groups', [
            'apiPrefix',
            '$resource',
            function( apiPrefix, $resource){
                return $resource( apiPrefix + 'groups/:gid', {gid: '@id'}, {
                    update: {
                        method: 'PUT'
                    }
                });
            }
        ]);