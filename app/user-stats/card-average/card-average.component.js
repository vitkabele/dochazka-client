/**
 * Slouží k zobrazení karty s denním průměrem
 * Parametry:
 * - uid:   User ID (Moment)
 * - start: Start date (moment)
 * - end:   end date (moment)
 * 
 * @author Vit Kabele <vit@kabele.me>
 */
'use strict';

angular
        .module("userStats")
        .component("cardAverage", {
            templateUrl: "app/user-stats/card-average/card-average.template.html",
            bindings: {
                user: '=',
                start: '=',
                end: '='
            },
            controller: [
                'statsStorage',
                function (statsStorage) {
                    this.statsStorage = statsStorage;
                }
            ]
        })
        .filter("averageCounter", [
    'holidays',
    function (holidays) {
            return function (stats) {
                var total = 0, d = 0, hours, minutes;

                if (stats.data !== undefined)
                    stats.data.forEach(function (item) {
                        // Pokud není dovolená && není víkend && není svátek
                        if (!item.attributes.vacation && moment(item.id).isoWeekday() < 6 && holidays.indexOf( moment(item.id).format('D.M.') ) === -1 ) {
                            total += item.attributes.total;
                            d++;
                        }
                    });
                
                return total/d;
            };
        }]);
