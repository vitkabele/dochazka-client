/**
 * @author Vit Kabele <vit@kabele.me>
 */
'use strict';

var module = angular.module('userStats');
//
//  Ze seznamu uživatelů vyfiltruje maintainery
//
module.filter("noMaintainer", function () {
    return function (data) {

        var filtered = [];

        data.forEach(function (item) {

            if (item != undefined && !item.attributes.maintainer)
                filtered.push(item);

        });

        return filtered;
    };
});

module.filter("strToMoment", function () {
    return function (data) {

        return moment(data);
    };
});

module.filter("sumLogin", function () {
    return function (data) {
        if (data !== undefined && data.stats.data !== undefined) {
            var total = 0;

            data.stats.data.forEach(function (item) {
                total += Number(item.meta.logged);
            })
            return total;
        }
        else
            return 0;
    };
});

module.filter("userIdToName", function (usersStorage) {
    return function (data) {

        return usersStorage.getUser(data).data.attributes.name;
    };
});

module.filter("groupIdToName", function (groupsStorage) {
    return function (data) {

        return groupsStorage.getGroup(data).attributes.name;
    };
});

module.filter("loadSummarizer",
    function (holidays) {

        return function (data, load, start, end) {

            if (!start.isSame(end, 'month') || data.data === undefined)
                return "N/A";

            var total = 0, w_days = 0;

            data.data.forEach(function (item) {
                if (item.attributes.vacation || moment(item.id).isoWeekday() > 5 || holidays.indexOf(moment(item.id).format('D.M.')) > -1)
                    return 0;

                total += item.attributes.total;
                w_days++;

            });

            var realLoad = (load / 20) * w_days,
                done = moment.duration(total, 'seconds').asHours() / realLoad;

            return Math.round(done * 100);
        };
    });

module.filter("loadInformator", function (holidays) {
    return function (data, load, start, end) {

        if (!start.isSame(end, 'month') || data.data === undefined)
            return "N/A";

        var total = 0, w_days = 0;

        data.data.forEach(function (item) {
            if (item.attributes.vacation || moment(item.id).isoWeekday() > 5 || holidays.indexOf(moment(item.id).format('D.M.')) > -1)
                return 0;

            total += item.attributes.total;
            w_days++;

        });

        var realLoad = (load / 20) * w_days;

        return Math.round(moment.duration(total, 'seconds').asHours() * 10) / 10 + 'h z ' + Math.round(realLoad * 10) / 10 + ' v ' + w_days + ' pracovních dnech';
    };
});
//
//  Returns data total in seconds
//
module.filter("total", function () {
    return function (data) {
        var total = 0;
        if (data.data !== undefined)
            data.data.forEach(function (item) {
                total += item.attributes.total;
            });
        return total;
    };
});
//
//  Returns human readable format of time interval in seconds
//
module.filter("humanReadable", function () {

    return function (seconds) {
        var m = moment.duration(seconds, 'seconds'),
            days = Math.floor(m.as('hours') / 8.5),
            hours = m.get('hours'),
            minutes = m.get('minutes');

        if (days > 0)
            return days + "d " + Math.floor(m.as('hours') % 8.5 * 10) / 10 + "h"
        else
            return hours + "h " + minutes + "m";
    };

});

module.filter("stats", function () {
    return function (storage, user, start, end) {
        if (!user || !start || !end)
            return {data: []};
        return storage.getStats(user, start, end);
    };
});

module.filter("length", function () {

    return function (data) {
        if (data.data !== undefined)
            return data.data.length;
    };

});

module.filter("data", function () {

    return function (data) {
        return data.data
    }

});

module.filter("groupFilter", function () {

    return function (users, filter) {

        var ordered = [];

        filter.forEach(function (item) {
            ordered.push(
                users.filter(function (obj) {
                    return obj.id == item;
                })[0]
            );
        });

        return ordered;

    };

});

module.filter("endOfWeek", function () {

    return function (date) {
        return (moment(date).isoWeekday() === 7);
    }

});


module.filter("isWeekend", function () {

    return function (date) {
        return (moment(date).isoWeekday() > 5);
    }

});

//
//  Formátuje moment podle řetězce
//
module.filter("moment", function () {

    return function (_time, _format) {
        return moment(_time).format(_format);
    };

})
//
//  Formátuje moment.duration podle řetězce
//
module.filter("duration", function () {
    return function (_duration, _format, _unit) {
        _unit = _unit || 'seconds';
        return moment.duration(_duration, _unit).get(_format);
    };
})