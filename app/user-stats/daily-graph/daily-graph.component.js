'use strict';

angular
        .module("userStats")
        .component("dailyGraph", {
            templateUrl: "app/user-stats/daily-graph/daily-graph.template.html",
            controller: [
                '$scope',
                'statsStorage',
                'displayPreferences',
                'graphSummarizer',
                function ($scope, statsStorage, displayPreferences, graphSummarizer) {

                    this.elapsedTime = Math.round(statsStorage.stats.meta.time_elapsed * 1000) / 1000;
                    this.displayPreferences = displayPreferences;

                    var chartData = {
                        data: {
                            labels: [],
                            series: [[]]
                        },
                        options: {
                            height: "270px",
                            plugins: [
                                Chartist.plugins.tooltip()
                            ]
                        },
                        responsiveOptions: [
                            ['screen and (max-width: 640px)', {
                                    seriesBarDistance: 5,
                                    axisX: {
                                        labelInterpolationFnc: function (label) {
                                            return moment(label).format('dd');
                                        }
                                    }
                                }]
                        ]
                    };
                    var chart = new Chartist.Bar('#chartActivity', chartData.data, chartData.options, chartData.responsiveOptions);

                    var chartUpdate = function () {
                        switch (displayPreferences.graphSummary) {
                            case 'daily':
                                chart.update(graphSummarizer.daily(statsStorage.stats.data));
                                break;
                            case 'weekly':
                                chart.update(graphSummarizer.weekly(statsStorage.stats.data));
                                break;
                            case 'monthly':
                                chart.update(graphSummarizer.monthly(statsStorage.stats.data));
                                break;
                        }
                        ;
                        chart.update();
                    };
                    //
                    //  Aktualizuje graf po změně jednotky zobrazení
                    //
                    $scope.$watch(function () {
                        return displayPreferences.graphSummary;
                    }, chartUpdate);
                    //
                    //  Aktualizuje graf po přijetí nových statistik
                    //
                    $scope.$on('statsUpdated', chartUpdate);

                }]
        });
