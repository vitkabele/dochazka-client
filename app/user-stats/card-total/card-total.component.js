'use strict';

angular
        .module("userStats")
        .component("cardTotal", {
            templateUrl: "app/user-stats/card-total/card-total.template.html",
            bindings: {
                user: '=',
                start: '=',
                end: '='
            },
            controller: [
                'statsStorage',
                function (statsStorage) {
                    this.statsStorage = statsStorage;
                }
            ]
        })
        .filter("hoursSummarizer", function () {
            return function (stats) {
                var total = 0;

                if (stats.data !== undefined)
                    stats.data.forEach(function (item) {
                        total += item.attributes.total;
                    });

                return total;
            };
        });
