/**
 * @author Vit Kabele <vit@kabele.me>
 */
angular
        .module("userStats")
        .component("cardDone",{
            templateUrl: "app/user-stats/card-done/card-done.template.html",
            bindings: {
                user:   '=',
                start:  '=',
                end:    '='
            },
            controller: [
                'statsStorage',
                function(statsStorage){
                    this.statsStorage = statsStorage;
                }]
        });
