'use strict';

angular
        .module('userStats')
        .component('tableDetails', {
            templateUrl: 'app/user-stats/table-details/table-details.template.html',
            bindings: {
                user:   '=',
                start:  '=',
                end:    '='
            },
            controller: [
                '$uibModal',
                'statsStorage',
                '$rootScope',
                'records',
                function ($uibModal, statsStorage, $rootScope, records) {

                    var self = this;
                    this.statsStorage = statsStorage;


                    this.openModal = function ( date, userId) {

                        $uibModal.open({
                            templateUrl: 'app/user-stats/table-details/edit-modal.template.html',
                            size: 'lg',
                            controller: [
                                '$scope',
                                '$uibModalInstance',
                                'dailyStats',
                                'exitReasonTranslate',
                                '$rootScope',
                                'records',
                                'statsStorage',
                                function ($scope, $uibModalInstance, dailyStats, translateTable, $rootScope, records, statsStorage) {

                                    var changelog = {};
                                    var deleted = [];

                                    // Neu je pole, do kterého se ukládají objekty nově vytvořených záznamů
                                    $scope.neu = [];

                                    $scope.addRecord = function () {

                                        $scope.neu.push({
                                            time: moment().toDate(),
                                            direction: 1,
                                            type: 0
                                        });

                                    };

                                    $scope.deleteRecord = function (item) {
                                        var index = $scope.neu.indexOf(item);
                                        $scope.neu.splice(index, 1);
                                    }

                                    $scope.deleteUnit = function (item) {
                                        deleted.push(item.attributes.start_id);
                                        deleted.push(item.attributes.end_id);

                                        var index = $scope.units.indexOf(item);
                                        $scope.units.splice(index, 1);

                                    }


                                    // Tabulka směrů
                                    $scope.typeTable = [
                                        "Odchod", //0
                                        "Příchod"    //1
                                    ];
                                    $scope.translateTable = translateTable;

                                    dailyStats.get({userId: userId, date: date}, function (response) {

                                        if( response.data !== undefined )
                                        response.data.forEach(function (item) {
                                            item.attributes.start = moment(date + ' ' + item.attributes.start).toDate();
                                            item.attributes.end = moment(date + ' ' + item.attributes.end).toDate();
                                        })

                                        $scope.units = response.data;
                                    })


                                    $scope.changeRecord = function (rid, time, type) {

                                        if (!changelog[rid])
                                            changelog[rid] = {
                                                id: rid,
                                                type: "record",
                                                _userId: userId,
                                                attributes: {
                                                    edited: {
                                                        type: 'employee',
                                                        id:   $rootScope.user.id,
                                                        time: moment().format("YYYY-MM-DD HH:mm:ss")
                                                    }
                                                }
                                            };

                                        if (time)
                                            changelog[rid].attributes.time = moment(time).format('YYYY-MM-DD HH:mm:ss');

                                        if (type !== undefined)
                                            changelog[rid].attributes.type = type;

                                    }

                                    $scope.save = function () {

                                        // Nové záznamy
                                        var toSave = [];

                                        $scope.neu.forEach(function (item) {

                                            toSave.push({
                                                type: "record",
                                                attributes: {
                                                    time: moment(date).format("YYYY-M-D") + ' ' + moment(item.time).format('HH:mm:ss'),
                                                    direct: item.direction,
                                                    type: item.type,
                                                    edited: {
                                                        type: 'employee',
                                                        id:   $rootScope.user.id,
                                                        time: moment().format("YYYY-MM-DD HH:mm:ss")
                                                    }
                                                }
                                            });

                                        });

                                        if (toSave.length > 0) {
                                            var requestObject = new records({data: toSave, _userId: userId});

                                            requestObject.$save(function (response) {
                                                $rootScope.$broadcast('statsReloadDemand');
                                                statsStorage.refresh(userId);
                                            });
                                        }

                                        // Editované záznamy
                                        for (var key in changelog) {
                                            records.update(changelog[key], function (response) {
                                                $rootScope.$broadcast('statsReloadDemand');
                                                statsStorage.refresh(userId);
                                            })
                                        }

                                        // Smazané záznamy
                                        deleted.forEach(function (item) {
                                            if (item)
                                                records.delete({
                                                    rid: item,
                                                    userId: userId
                                                }, function (response) {
                                                    $rootScope.$broadcast('statsReloadDemand');
                                                    statsStorage.refresh(userId);
                                                }, function (response) {
                                                    $rootScope.$broadcast('statsReloadDemand')
                                                })

                                        })


                                        $uibModalInstance.close()
                                    }

                                    // Zavře dialog bez uložení
                                    $scope.cancel = function () {
                                        $uibModalInstance.dismiss();
                                    };

                                    $scope.date = date;

                                }],
                            resolve: {
                            }
                        });
                    };

                    this.AddFullTime = function (date, userId) {
                      var ToSave = [
                        {
                          type: "record",
                          attributes: {
                            time: moment(date).format("YYYY-MM-DD") + ' ' + "01:23:45",
                            direct: 1,
                            type: 0,
                            edited: {
                                type: 'employee',
                                id:   $rootScope.user.id,
                                time: moment().format("YYYY-MM-DD HH:mm:ss")
                            }
                          }
                        },{
                          type: "record",
                          attributes: {
                            time: moment(date).format("YYYY-MM-DD") + ' ' + "09:53:45",
                            direct: 0,
                            type: 0,
                            edited: {
                                type: 'employee',
                                id:   $rootScope.user.id,
                                time: moment().format("YYYY-MM-DD HH:mm:ss")
                            }
                          }
                        }
                      ];

                      var requestObject = new records({data: ToSave, _userId: userId});

                      requestObject.$save(function (response) {
                          $rootScope.$broadcast('statsReloadDemand');
                          statsStorage.refresh(userId);
                      });

                    }
                }
            ]
        })
        .filter("proColumn", function () {
            return function (count) {
                return Math.ceil(count / 3)
            };
        })
        .filter("isDoctor", function () {
            return function (item) {
                var accu = {};
                item.meta.reasons.reduce(function (acc, curr) {

                    accu[curr] ? accu[curr]++ : accu[curr] = 1;

                }, {});

                // Kvůli doktorským srdíčkům
                return !!accu[201];
            };
        })
                .filter('isHoliday', [
            'holidays',
            function(holidays){
                    return function( id ){
                        return (holidays.indexOf( moment( id ).format('D.M.') ) > -1 );
                    };
                }]);
