'use strict';

angular
        .module("fiedlerDochazka")

        /*
         * Modul: entryPoint
         * Stará se o přesměrování uživatelů na jejich nástěnky
         */
        .component('entryPoint', {
            templateUrl: "app/login.html",
            controller: [
                '$rootScope',
                '$resource',
                '$cookies',
                '$state',
                'jwtHelper',
                'userProvider',
                function ($rootScope, $resource, $cookies, $state, jwtHelper, userProvider) {
                    //
                    //  Obstarává přesměrování po přihlášení
                    //
                    var redirector = function (loggedUser) {
                        //
                        // Admin && Maintainer => Na přehled všech uživatelů
                        //
                        if (loggedUser.attributes.maintainer && loggedUser.attributes.admin)
                            $state.go('stats.overview');
                        //
                        // !Admin && Maintainer => Na Doorman
                        //
                        else if(!loggedUser.attributes.admin && loggedUser.attributes.maintainer)
                            $state.go("doorman");
                        //
                        // Pro případy ( Admin && !Maintainer ) || (!Admin && !Maintainer) se přesměrovává vždy na vlastní statistiky
                        //
                        else
                            $state.go('stats.dashboard', {viewedUserId: loggedUser.id});
                    };

                    // Když je platný api token, nenechá zobrazit login Page
                    if ($cookies.get('apiToken') && !jwtHelper.isTokenExpired($cookies.get('apiToken'))) {
                        $rootScope.user = userProvider.get({
                            userId: jwtHelper.decodeToken($cookies.get('apiToken')).id},
                                function (response) {
                                    $rootScope.user = response.data;
                                    redirector(response.data);
                                });
                    }


                    var self = this;
                    this.errors = [];

                    var userService = $resource('/login');

                    self.user = new userService;

                    self.user.data = {
                        identity: "",
                        password: ""
                    };

                    this.login = function () {

                        self.user.$save(function (response) {
                            $cookies.put('apiToken', response.token);
                            $rootScope.user = response.data;
                            $rootScope.viewedUserId = $rootScope.user.id;
                            redirector(response.data);
                        },
                                function (response) {
                                    self.errors = response.data.errors;
                                });

                    };
                }]
        })

        /**
         * Modul: errorView
         * Stará se o zobrazní errorů v lidsky čitelné podobě
         */
        .component('errorView', {
            template: "<h1>V aplikaci došlo k problému</h1> <p>{{$ctrl.errorMessage}}</p><p>Zpět se vrátíte kliknutím na <a data-ui-sref=\"stats({userId: 1})\">Tento odkaz</a></p>",
            controller: function ($stateParams) {
                this.errorMessage = $stateParams.eid;
            }
        })