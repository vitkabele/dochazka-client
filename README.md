# Obecné poznámky

## Hierarchie uživatelských oprávnění

Oprávnění jsou ovlivněna dvěma hodnotami (admin & maintainer)

 - a=0, m=0 => Běžný uživatel, vidí pouze svojí nástěnku, nemůže editovat, ani do doormanu
 - a=0, m=1 => Nemá svojí nástěnku, ale nevidí ani cizí. Má přístup do doormanu
 - a=1, m=0 => Uživatel správce, je jako běžný uživatel + vidí všechny uživatele systému, firemní a denní nástěnku, doorman, může editovat
 - a=1, m=1 => Uživatel který nemá svojí docházku, ale vidí to co admin a může editovat

## $rootScope
Pozor na znečištění! Všechny věci uložené do globálního $scope musí být zdokumentované tady:

- __userID__ -> Id uživatele z url. Je ve scope, aby se dalo sledovat
- __viewedUserId__ -> id prohlíženého uživatele
- __user__ --- Objekt aktuálně přihlášeného uživetele.

## Struktura aplikace

Aplikace je napsána v angular JS frameworku.

Celek je organizován do modulů následovně:

```

  ../fiedlerDochazka M
    |-- C: Entry
    |-- C: Error
    |
    |-- M: userStats
        |-- V: statsStorage
        |-- S: periodStorage
        |-- C: dashboard
            |-- C: daily-graph
            |-- C: card-total
            |-- C: card-average

    Legenda: M - modul, C - komponenta, V - value, S - service

```
Zobazení nemusí nutně odpovídat adresářové struktuře projektu.
Zobrazení je spíše schematické. Componenty pod dashboard samozřejmě náleží modulu userStats.

Filtry:
 - vacationSummarizer   - Pro kartu s dovolenou
 - hoursSummarizer      - Pro kartu s total timem
 - averageCounter       - Pro kartu s průměrem
 - errorCountTotal      - Pro kartu s chybami
 - errorCountDays       - Pro kartu s chybami
 - tableReasons

### M: fiedlerDochazka
Hlavní modul aplikace. Jako jádro uchovává některé klíčové informace, které jsou klíčové pro běh aplikace, nebo jejich factory.

### C: Entry
Toto je jednoduchá komponenta, která má jen controller a šablonu, slouží k designově vyhovujícímu zadávání 'přihlašovacích' údajů.

### M: userStats
Obsahuje celou část aplikace s nástěnkou.

#### C: dashboard
Komponenta modulu userStats, jedná se o základní komponentu, jejíž název je uveden jako šablona v _stateProvider_. Šablona obsahuje celý pohled nástěnky včetně horní a postranní lišty.

viewedUserId nabývá hodnot když:
 - Se uživatel přihlásí. V takovém případě je totžná s loggedUser.
 - Dojde ke změně stavu aplikace. V takovém případě je její hodnota buď loggedUser (když je stav stats), nebo podle parametru __:userId__, když je stav na statistikách někoho jiného.

Když dojde ke změně _$scope.viewedUserId_ zavolá se zde _$watch_ a do _value_ __statsStorage__ uloží objekt statistik, který se vrátí ze serveru.

Po změně prohlíženého uživatele se na __$rootScope__ broadcastuje událost, která jako paramtr dostane objekt se statistikami. Tím odpadá potřeba globálního úložiště statistik.

##### S: periodStorage
Obsahuje začátek a konec období pro zobrazení + metody pro jejich nastavování

#### C: daily-graph
Jedná se též o komponentu modulu userStats, ale už má na starosti konkréní _kartu_ zobrazení. Konkrétně jde o zobrazení grafu závislosti hodin na dni. Vyžaduje provider chartist.

#### C: card-total
Zobrazuje kartu se souhrnem hodin za dané období

#### C: card-average
Zobrazuje kartu s průměrem za dané období


## Contact

For more information on AngularJS please check out http://angularjs.org/

[git]: http://git-scm.com/
[bower]: http://bower.io
[npm]: https://www.npmjs.org/
[node]: http://nodejs.org
[protractor]: https://github.com/angular/protractor
[jasmine]: http://jasmine.github.io
[karma]: http://karma-runner.github.io
[travis]: https://travis-ci.org/
[http-server]: https://github.com/nodeapps/http-server